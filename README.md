# URL internationalization helper

This is an interactive Node.js command-line tool that helps you add URLs to Wikidata items.

## What does this program do?

### Background

Many websites offer multiple languages that are available by adjusting the URL path, e.g. the American English version of Minecraft.net is at <https://www.minecraft.net/en-us>, or you can view the German version at <https://www.minecraft.net/de-de>. Wikidata uses the [language of work or name](https://www.wikidata.org/wiki/Property:P407) qualifier to specify the language of a URL, but adding each possible URL with the corresponding qualifier manually is time-consuming and error-prone: in the example of Minecraft.net, 21 statements would need to be added by hand!

Fortunately, many websites present the list of available language variants in a machine-readable format with the `<link rel="alternate" hreflang=*>` HTML element (see documentation [from Google](https://developers.google.com/search/docs/specialty/international/localized-versions#html) or [MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/rel#alternate)). This means that a script can be given a single URL, and quickly find the other URLs that give access to translated versions of the page.

## To-do list

### User interface

The user interface (UI) parts of the code gather input from the user to determine which item to edit, the URL to use for language-variant lookups, and which property to add the URLs to. It presents the data it has gathered in a intuitive and attractive format, to let the user confirm that it is making the correct edits. It also shows progress, status and error messages to keep the user informed.

### Wikidata backend

The other half of the codebase is dedicated to editing Wikidata through its API, and contains all the logic required for property lookups, authentication and adding statements.
